package Screens;


import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;



public class LoginScreen  {

     AndroidDriver driver;

//    public LoginScreen(AndroidDriver driver)
//    {
//        super(driver);
//        System.out.println(" Login screen constructor call");
//    }
    public LoginScreen(AndroidDriver driver){
        this.driver=driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }


    // 1. By Locators

//    @FindBy(id ="com.pataa.dev:id/edtMobileNumber")
//    private   MobileElement mobilenoinput;
//
//    @FindBy(id="com.pataa.dev:id/btn")
//    private MobileElement sendotpbutton;
//
//    @FindBy(id="com.pataa.dev:id/tvLanguage")
//    private MobileElement language;
//
//    @FindBy(id="com.pataa.dev:id/tvTandCMessage")
//    private MobileElement privacypolicyandtermofuse;
//
//    @FindBy(id="com.pataa.dev:id/screenTitle")
//    private MobileElement firsttitle;
//
//    @FindBy(id="com.pataa.dev:id/screenTitle2")
//    private MobileElement secondtitle;
//
//
//    @FindBy(id="com.pataa.dev:id/screenTitle3")
//    private MobileElement thirdtitle;
//
//    @FindBy(id="com.pataa.dev:id/screenContent")
//    private MobileElement pataaplatebelowcontent;
//
//    @FindBy(id="com.pataa.dev:id/tvBenefits1")
//    private MobileElement benefits1;
//
//    @FindBy(id="com.pataa.dev:id/tvBenefits2")
//    private MobileElement benefits2;
//
//    @FindBy(id="com.pataa.dev:id/tvBenefits3")
//    private MobileElement benefits3;
//
//    @FindBy(id="com.pataa.dev:id/tvError")
//    private MobileElement mobilemsg;


    private By mobilenoinput = By.id("com.pataa.dev:id/edtMobileNumber");
    private By sendotpbutton = By.id("com.pataa.dev:id/btn");
    private By language = By.id("com.pataa.dev:id/tvLanguage");
    private By privacypolicyandtermofuse = By.id("com.pataa.dev:id/tvTandCMessage");
    private By firsttitle = By.id("com.pataa.dev:id/screenTitle");
    private By secondtitle = By.xpath("com.pataa.dev:id/screenTitle2");
    private By thirdtitle = By.xpath("com.pataa.dev:id/screenTitle3");
    private By pataaplatebelowcontent = By.id("com.pataa.dev:id/screenContent");
    private By benefits1 = By.id("com.pataa.dev:id/tvBenefits1");
    private By benefits2 = By.id("com.pataa.dev:id/tvBenefits2");
    private By benefits3 = By.id("com.pataa.dev:id/tvBenefits3");
    private By mobilemsg = By.id("com.pataa.dev:id/tvError");




    public void enterMobileNoWithClick (String mono){
        driver.findElement(mobilenoinput).click();
    }
    public void enterMobileNoOnly(String mono){
        driver.findElement(mobilenoinput).click();
    }

    public void tapSendOTPButton(){

    }

    public String checkFirstTitleExist(){
      return  driver.findElement(firsttitle).getText();
    }
    public String checkSecondTitleExist(){
       return driver.findElement(thirdtitle).getText();

    }
    public String checkThirdTitleExist(){
        return driver.findElement(thirdtitle).getText();

    }

    public String checkBenefit1(){
      return driver.findElement(benefits1).getText();

    }
    public String checkBenefit2(){
      return driver.findElement(benefits2).getText();

    }
    public String checkBenefit3(){
        return driver.findElement(benefits3).getText();

    }

    public String checkpataaplatebelowcontent(){
        return driver.findElement(pataaplatebelowcontent).getText();
    }

    public String checkmobilemsg(){
       return driver.findElement(mobilemsg).getText();
    }





}
