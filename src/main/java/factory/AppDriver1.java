package factory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.URL;


public class AppDriver1 {

    private static ThreadLocal<AndroidDriver> driver = new ThreadLocal<>();

    public static AndroidDriver getDriver(URL url, DesiredCapabilities caps){

        return driver.get();
    }

    public static void setDriver(AndroidDriver Driver){
        driver.set(Driver);
        System.out.println("Driver is set");
    }
}
