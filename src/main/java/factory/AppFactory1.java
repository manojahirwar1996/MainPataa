package factory;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.remote.MobileCapabilityType;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.PageFactory;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class AppFactory1 {
    protected AndroidDriver driver;

      public static AndroidDriver Android_LaunchApp() {

          AndroidDriver driver = null;

        try {
            DesiredCapabilities cap1 = new DesiredCapabilities();
            cap1.setCapability(MobileCapabilityType.PLATFORM_NAME, "ANDROID");
            cap1.setCapability(MobileCapabilityType.PLATFORM_VERSION, "11");
            cap1.setCapability(MobileCapabilityType.DEVICE_NAME, "sdk_gphone_x86");
            cap1.setCapability(MobileCapabilityType.UDID, "emulator-5554");
            cap1.setCapability("appPackage", "com.pataa.dev");
            cap1.setCapability("appActivity", "com.pataa.ui.splash.SplashMainActivity");

            URL url = new URL("http://127.0.0.1:4723/wd/hub");
            driver = new AndroidDriver(url, cap1);
            System.out.println("Application started....");
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            // factory.AppDriver.setDriver(driver);
            //
        } catch (MalformedURLException ex){
            ex.printStackTrace();
        }
        return driver;


    }
}











