
import Screens.LoginScreen;
import factory.AppFactory1;
import io.appium.java_client.android.AndroidDriver;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.testng.Assert;


public class LoginScreenStepdefs
{

    AndroidDriver driver;


    @Given("user is on login screen")
    public void user_is_on_login_screen()  {

        driver = AppFactory1.Android_LaunchApp();

    }

    @Then("Verify the blue screen content {string} {string} and {string}")
    public void verify_the_blue_screen_content(String ar1,String arg2, String arg3) {
        // Write code here that turns the phrase above into concrete actions

        LoginScreen loginScreen = new LoginScreen(driver);

        String btext = loginScreen.checkFirstTitleExist();
        System.out.println(btext);
        Assert.assertEquals(ar1,btext );

    }

    @When("user swipes left")
    public void user_swipes_left() {

    }

  @Then("Verify the yellow screen content {string} and {string}")
    public void verify_the_yellow_screen_content(String ar1,String arg2) {

    }

  @Then("Verify the green screen content {string} and {string}")
    public void verify_the_green_screen_content(String ar1,String arg2) {
    }

    @When("user enters mobile no {string}")
    public void user_enters_mobile_no(String string) {

    }

    @When("user clicks on Login button")
    public void user_clicks_on_login_button() {

    }

    @When("user enters OTP {string}")
    public void user_enters_otp(String string) {

    }

    @Then("user gets the title of the page")
    public void user_gets_the_title_of_the_page() {
        // Write code here that turns the phrase above into concrete actions

    }

    @Then("screen title should be {string}")
    public void screen_title_should_be(String string) {
        // Write code here that turns the phrase above into concrete actions
    }


  }

