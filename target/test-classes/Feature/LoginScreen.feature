Feature: Login screen feature

  Scenario: Verify the callouts content
      Given user is on login screen
      Then Verify the blue screen content "create" "digital address code" and "for your location"
      When user swipes left
      Then Verify the yellow screen content "record voice directions" and "in your own voice"
      When user swipes left
      Then Verify the green screen content "share address easily" and "precise upto 3 meters"

# Scenario: Login with correct credentials
#    Given user is on login screen
#    When user enters mobile no "2348354342"
#    And user clicks on Login button
#    And user enters OTP "54342"
#    Then user gets the title of the page
#    And screen title should be "Search pataa"



